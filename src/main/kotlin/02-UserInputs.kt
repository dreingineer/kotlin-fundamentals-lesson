import java.util.Scanner

fun main() {
  // Call user input 1
  userInput1()
  // Call user input 2
  userInput2()
  // Call user input 3
  userInput3()
}

fun userInput1() {
  println("What is your professional goal?")

  val myGoal = readLine()
  println("My professional goal is: $myGoal")
}

fun userInput2() {
  println("What is your favorite number?")

  val favoriteNumber = Integer.valueOf(readLine())
  println("My favorite number is: $favoriteNumber")
}

fun userInput3() {
  val readScanner = Scanner(System.`in`)

  println("What language is your favorite so far?")
  var language = readScanner.next()

  println("How many years have you used that language?")
  var howLong = readScanner.nextInt()

  println("My favorite language is $language")
  println("I've been using that for $howLong years")
}